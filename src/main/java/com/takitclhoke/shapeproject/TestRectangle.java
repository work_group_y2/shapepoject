/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(2);
        System.out.println("Area of rectangle1(s = " + rectangle1.getS() + ") is " + rectangle1.recArea());
        rectangle1.setS(1); // rectangle1.s = 1;
        System.out.println("Area of rectangle1(s = " + rectangle1.getS() + ") is " + rectangle1.recArea());
        rectangle1.setS(0);
        System.out.println("Area of rectangle1(s = " + rectangle1.getS() + ") is " + rectangle1.recArea());
        System.out.println(rectangle1.toString());
        
    }
            
}
