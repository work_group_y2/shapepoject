/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Triangle {
    private double n;
    private double b;
    public static final double oh = 1.0/2;
    public Triangle(double n, double b){
        this.n = n;
        this.b = b;
    }
    public double triArea() {
        return oh * b * n;
    }
    public double getN() {
        return n;
    }
    public double getB() {
        return b;
    }
    //public void setN(double n) {
    //   if(n <= 0) {
    //      System.out.println("Error: Redius must more than zero!!!!");
    //      return;
    // }
    //   this.n = n;
    //}
    //public void setB(double b) {
    //    if(b <= 0) {
    //        System.out.println("Error: Redius must more than zero!!!!");
    //        return;
    //    }
    //    this.b = b;
    //}
    public void setNB(double n, double b){
        if(n <= 0 || b <= 0){
            System.out.println("Error: Redius must more than zero!!!!");
            return;
        }
        this.n = n;
        this.b = b;
    }
    
    
    
    @Override
    public String toString() {
        return "Area of triangle1(n = " + this.getN() + ",b = " + this.getB() + ") is " + this.triArea();
    }
}
