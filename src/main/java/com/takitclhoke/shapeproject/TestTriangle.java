/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3, 4);
        System.out.println("Area of triangle1(n = " + triangle1.getN() + ",b = " + triangle1.getB() + ") is " + triangle1.triArea());
        //triangle1.setN(2); //triangle1.n = 2;
        //triangle1.setB(3); //triangle1.n = 3;
        triangle1.setNB(2, 3); 
        System.out.println("Area of triangle1(n = " + triangle1.getN() + ",b = " + triangle1.getB() + ") is " + triangle1.triArea());
        //triangle1.setN(0);
        //triangle1.setB(0);
        triangle1.setNB(0, 0); 
        System.out.println("Area of triangle1(n = " + triangle1.getN() + ",b = " + triangle1.getB() + ") is " + triangle1.triArea());
        System.out.println(triangle1.toString());
        System.out.println(triangle1);
    }
}
