/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestSquara {
    public static void main(String[] args) {
        Squara squara1 = new Squara(3, 4);
        System.out.println("Area of squara1(w = " + squara1.getW() + ",h = "  + squara1.getH() + ") is " + squara1.squArea());
        //squara1.setW(2); //squara1.w = 2;
        //squara1.setH(3); //squara1.h = 3;
        squara1.setWH(2,3);
        System.out.println("Area of squara1(w = " + squara1.getW() + ",h = "  + squara1.getH() + ") is " + squara1.squArea());
        //squara1.setW(0);
        //squara1.setH(0);
        squara1.setWH(0,0);
        System.out.println("Area of squara1(w = " + squara1.getW() + ",h = "  + squara1.getH() + ") is " + squara1.squArea());
        System.out.println(squara1.toString());
        
    }
}
