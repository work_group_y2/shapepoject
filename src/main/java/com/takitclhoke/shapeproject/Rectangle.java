/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Rectangle {
    private double s;
    public Rectangle(double s) {
        this.s = s;
    }
    public double recArea() {
        return s * s;
    }
    public double getS() {
        return s;
    }
    public void setS(double s){
        if(s <= 0){
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.s = s;
    }
    public String toString() {
        return "Area of rectangle1(s = " + this.getS() + ") is " + this.recArea();
    }
        
}
